package com.aswdc.gpsc.Fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import static com.aswdc.gpsc.Activity.PracticeActivity.question_array;

import com.aswdc.gpsc.Activity.PracticeActivity;
import com.aswdc.gpsc.Bean.BeanPractice;
import com.aswdc.gpsc.R;
import com.aswdc.gpsc.Database.DBPractice;


public class PracticeFragment extends Fragment {

    TextView tvQuestionNumber, tvQuestion;
    RadioButton rb_A;
    RadioButton rb_B;
    RadioButton rb_C;
    RadioButton rb_D;
    RadioGroup rg;
    ImageView imgRevision;
    ViewPager vPage;
    DBPractice dbp;
    BeanPractice bpractice;
    int count = 0;

    public static String strChecked = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_practice, container, false);
        Bundle args = getArguments();
        count = args.getInt("page_position");
        bpractice = question_array.get(count);
        final BeanPractice fbq = question_array.get(count);
        dbp = new DBPractice(getActivity());

        tvQuestionNumber = (TextView) view.findViewById(R.id.practice_fragment_tv_question_number);
        tvQuestion = (TextView) view.findViewById(R.id.practice_fragment_tv_question);
        rb_A = (RadioButton) view.findViewById(R.id.practice_fragment_rb_Option_A);
        rb_B = (RadioButton) view.findViewById(R.id.practice_fragment_rb_Option_B);
        rb_C = (RadioButton) view.findViewById(R.id.practice_fragment_rb_Option_C);
        rb_D = (RadioButton) view.findViewById(R.id.practice_fragment_rb_Option_D);
        rg = (RadioGroup) view.findViewById(R.id.practice_fragment_radiogroup);
        imgRevision = (ImageView) view.findViewById(R.id.practice_fragment_img_revision);
        vPage = (ViewPager) view.findViewById(R.id.practice_page);

        tvQuestionNumber.setText("Q." + (count + 1) + " of " + question_array.size());
        tvQuestion.setText(bpractice.getQuestion());
        rb_A.setText(bpractice.getOptionA());
        rb_B.setText(bpractice.getOptionB());
        rb_C.setText(bpractice.getOptionC());
        rb_D.setText(bpractice.getOptionD());

        if (bpractice.getAnsStatus().equalsIgnoreCase("a")) {
            if (bpractice.getTrueOption().equalsIgnoreCase("a")) {
                rb_A.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_A.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rb_A.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_A.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        } else if (bpractice.getAnsStatus().equalsIgnoreCase("b")) {
            if (bpractice.getTrueOption().equalsIgnoreCase("b")) {
                rb_B.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_B.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rb_B.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_B.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        } else if (bpractice.getAnsStatus().equalsIgnoreCase("c")) {
            if (bpractice.getTrueOption().equalsIgnoreCase("c")) {
                rb_C.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_C.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rb_C.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_C.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        } else if (bpractice.getAnsStatus().equalsIgnoreCase("d")) {
            if (bpractice.getTrueOption().equalsIgnoreCase("d")) {
                rb_D.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_D.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rb_D.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_D.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        }

        if (PracticeActivity.question_array.get(count).getIsFavourite() == 1) {
            imgRevision.setImageResource(R.mipmap.icon_fill_mark_revision);
            imgRevision.setTag("Dark=" + question_array.get(count).getQuestionID());

        } else {
            imgRevision.setImageResource(R.mipmap.icon_mark_revision);
            imgRevision.setTag("Light=" + question_array.get(count).getQuestionID());
        }

        imgRevision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tagName = imgRevision.getTag().toString();
                String str[] = tagName.split("=");
                tagName = str[0];
                String strID = str[1];

                if (tagName.equalsIgnoreCase("Light")) {
                    imgRevision.setImageResource(R.mipmap.icon_fill_mark_revision);
                    imgRevision.setTag("Dark=" + strID);
                    dbp.updateRev(1, bpractice.getQuestionID());
                    bpractice.setIsFavourite(1);
                } else {
                    imgRevision.setImageResource(R.mipmap.icon_mark_revision);
                    imgRevision.setTag("Light=" + strID);
                    dbp.updateRev(0, bpractice.getQuestionID());
                    bpractice.setIsFavourite(0);
                }
            }
        });


        rb_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_A.isChecked()) {
                    strChecked = "A";
                    click(strChecked);
                    bpractice.setAnsStatus("A");
                }
            }
        });
        rb_B.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (rb_B.isChecked()) {
                    strChecked = "B";
                    click(strChecked);
                    bpractice.setAnsStatus("B");
                }
            }
        });
        rb_C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_C.isChecked()) {
                    strChecked = "C";
                    click(strChecked);
                    bpractice.setAnsStatus("C");
                }
            }
        });
        rb_D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_D.isChecked()) {
                    strChecked = "D";
                    click(strChecked);
                    bpractice.setAnsStatus("D");
                }
            }
        });

        return view;
    }

    public void trueOption() {
        Bundle args = getArguments();
        count = args.getInt("page_position");
        if (PracticeActivity.question_array.get(count).getTrueOption() != null) {
            click(PracticeActivity.question_array.get(count).getTrueOption());
            bpractice.setAnsStatus(PracticeActivity.question_array.get(count).getTrueOption());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void click(String ans) {
        rb_A.setBackground(getResources().getDrawable(R.drawable.rounded_borders));
        rb_B.setBackground(getResources().getDrawable(R.drawable.rounded_borders));
        rb_C.setBackground(getResources().getDrawable(R.drawable.rounded_borders));
        rb_D.setBackground(getResources().getDrawable(R.drawable.rounded_borders));
        rb_A.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));
        rb_B.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));
        rb_C.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));
        rb_D.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));

        if (ans.equalsIgnoreCase(PracticeActivity.question_array.get(count).getTrueOption())) {
            if (ans.equalsIgnoreCase("A")) {
                rb_A.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_A.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rb_A.setChecked(true);
                rb_B.setEnabled(false);
                rb_C.setEnabled(false);
                rb_D.setEnabled(false);
            } else if (ans.equalsIgnoreCase("B")) {
                rb_B.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_B.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rb_B.setChecked(true);
                rb_C.setEnabled(false);
                rb_A.setEnabled(false);
                rb_D.setEnabled(false);
            } else if (ans.equalsIgnoreCase("C")) {
                rb_C.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_C.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rb_C.setChecked(true);
                rb_A.setEnabled(false);
                rb_B.setEnabled(false);
                rb_D.setEnabled(false);
            } else {
                rb_D.setBackground(getResources().getDrawable(R.drawable.border_green));
                rb_D.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rb_D.setChecked(true);
                rb_C.setEnabled(false);
                rb_B.setEnabled(false);
                rb_A.setEnabled(false);
            }
        } else {
            if (ans.equalsIgnoreCase("A")) {
                rb_A.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_A.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            } else if (ans.equalsIgnoreCase("B")) {
                rb_B.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_B.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            } else if (ans.equalsIgnoreCase("C")) {
                rb_C.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_C.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            } else {
                rb_D.setBackground(getResources().getDrawable(R.drawable.border_red));
                rb_D.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        }
    }
}
