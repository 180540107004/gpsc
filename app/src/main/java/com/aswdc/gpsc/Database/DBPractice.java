package com.aswdc.gpsc.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.gpsc.Bean.BeanPractice;
import com.aswdc.gpsc.Utility.Constant;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import java.util.ArrayList;

public class DBPractice extends SQLiteAssetHelper {

    public DBPractice(Context context) {
        super(context, Constant.DB_Name, null, null, Constant.DB_Version);
    }

    public ArrayList<BeanPractice> selectAllQuestion() {
        ArrayList<BeanPractice> arrayques = new ArrayList<BeanPractice>();

        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "Select QuestionID,Question,Option_A,Option_B,Option_C,Option_D,Answer,TrueOption,Favourite from MST_Question";
        Cursor cursor = db.rawQuery(strQuery, null);
        if (cursor.moveToFirst()) {
            do {
                BeanPractice bq = new BeanPractice();
                bq.setQuestionID(cursor.getInt(cursor.getColumnIndex("QuestionID")));
                bq.setQuestion(cursor.getString(cursor.getColumnIndex("Question")));
                bq.setOptionA(cursor.getString(cursor.getColumnIndex("Option_A")));
                bq.setOptionB(cursor.getString(cursor.getColumnIndex("Option_B")));
                bq.setOptionC(cursor.getString(cursor.getColumnIndex("Option_C")));
                bq.setOptionD(cursor.getString(cursor.getColumnIndex("Option_D")));
                bq.setAnswer(cursor.getString(cursor.getColumnIndex("Answer")));
                bq.setTrueOption(cursor.getString(cursor.getColumnIndex("TrueOption")));
                bq.setIsFavourite(cursor.getInt(cursor.getColumnIndex("Favourite")));
                arrayques.add(bq);
            } while (cursor.moveToNext());
        }
        db.close();
        return arrayques;
    }

    public ArrayList<BeanPractice> selectById(int categoryID) {

        SQLiteDatabase database = getReadableDatabase();
        String strQuery = " Select qn.CategoryID, Question, Option_A , Option_B , Option_C, Option_D,Answer,TrueOption,Favourite From MST_Question qn Inner Join MST_Category c on qn.CategoryID=c.CategoryID where c.CategoryID="+categoryID;

        Cursor cursor = database.rawQuery(strQuery, null);

        ArrayList<BeanPractice> questionList = new ArrayList<BeanPractice>();

        if (cursor.moveToFirst()) {
            do {
                BeanPractice bq = new BeanPractice();
                bq.setCategoryID(cursor.getInt(cursor.getColumnIndex("CategoryID"+"")));
                bq.setQuestion(cursor.getString(cursor.getColumnIndex("Question")));
                bq.setOptionA(cursor.getString(cursor.getColumnIndex("Option_A")));
                bq.setOptionB(cursor.getString(cursor.getColumnIndex("Option_B")));
                bq.setOptionC(cursor.getString(cursor.getColumnIndex("Option_C")));
                bq.setOptionD(cursor.getString(cursor.getColumnIndex("Option_D")));
                bq.setAnswer(cursor.getString(cursor.getColumnIndex("Answer")));
                bq.setTrueOption(cursor.getString(cursor.getColumnIndex("TrueOption")));
                bq.setIsFavourite(cursor.getInt(cursor.getColumnIndex("Favourite")));
                questionList.add(bq);
            }
            while (cursor.moveToNext());
        }
        database.close();
        return questionList;
    }

    public int getTabelRowCount(String tname) {

        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "Select * from " + tname;
        Cursor cur = db.rawQuery(strQuery, null);
        cur.moveToFirst();
        return cur.getCount();
    }

    public BeanPractice selectQuestionByID(int id) {

        BeanPractice bq = new BeanPractice();
        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "select Answer,TrueOption from MST_Question where QuestionID=" + id;
        Cursor cur = db.rawQuery(strQuery, null);

        if (cur.moveToFirst()) {
            bq.setAnswer(cur.getString(cur.getColumnIndex("Answer")));
            bq.setTrueOption(cur.getString(cur.getColumnIndex("TrueOption")));
        }
        db.close();
        return bq;
    }

    public void updateRev(int a, int id) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("Favourite", a);
        db.update("MST_Question", cv, "QuestionID=" + id, null);
        db.close();
    }
}
