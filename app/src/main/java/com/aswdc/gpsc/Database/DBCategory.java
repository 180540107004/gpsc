package com.aswdc.gpsc.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.gpsc.Bean.BeanCategory;
import com.aswdc.gpsc.Utility.Constant;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class DBCategory extends SQLiteAssetHelper {

    public DBCategory(Context context) {
        super(context, Constant.DB_Name, null, Constant.DB_Version);
    }

    public ArrayList<BeanCategory> selectCategory() {

        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "Select * FROM MST_Category";
        Cursor cursor = db.rawQuery(strQuery, null);
        ArrayList<BeanCategory> arrayCategory = new ArrayList<BeanCategory>();
        if (cursor.moveToFirst()) {
            do {
                BeanCategory bq = new BeanCategory();
                bq.setCategoryID(cursor.getInt(cursor.getColumnIndex("CategoryID")));
                bq.setCategoryName(cursor.getString(cursor.getColumnIndex("CategoryName")));
                bq.setCategoryNameGUJ(cursor.getString(cursor.getColumnIndex("CategoryNameGUJ")));
                arrayCategory.add(bq);
            } while (cursor.moveToNext());
        }
        db.close();
        return arrayCategory;
    }

}
