package com.aswdc.gpsc.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aswdc.gpsc.Activity.RevisionActivity;
import com.aswdc.gpsc.Database.DB_MarkRev;
import com.aswdc.gpsc.R;
import com.aswdc.gpsc.Bean.BeanPractice;
import com.aswdc.gpsc.Database.DBPractice;
import java.util.ArrayList;

public class AdapterMarkRevision extends BaseAdapter {

    ArrayList<BeanPractice> arrayQues;
    Activity act;
    DB_MarkRev dbmr;
    DBPractice dbp;
    BeanPractice bq;

    public AdapterMarkRevision(Activity act, ArrayList<BeanPractice> arrayQue) {
        this.act = act;
        this.arrayQues = arrayQue;
        dbmr = new DB_MarkRev(act);
        dbp = new DBPractice(act);
        bq = new BeanPractice();
    }

    @Override
    public int getCount() {
        return arrayQues.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayQues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView tvQueID;
        TextView tvQue;
        TextView tvans;
        ImageView imgRev;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = act.getLayoutInflater();
        Log.d("sizeArray",arrayQues.size()+"");
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.revision_marked, null);
            holder = new ViewHolder();

            holder.tvQueID = (TextView) convertView.findViewById(R.id.markrev_tv_id);
            holder.tvQue = (TextView) convertView.findViewById(R.id.markRev_tv_que);
            holder.tvans = (TextView) convertView.findViewById(R.id.markRev_tv_ans);
            holder.imgRev = (ImageView) convertView.findViewById(R.id.markRev_img_rev);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (arrayQues.get(position).getIsFavourite() == 1) {
            holder.imgRev.setImageResource(R.mipmap.icon_fill_mark_revision);
            holder.imgRev.setTag("Dark=" + arrayQues.get(position).getQuestionID());

        } else {
            holder.imgRev.setImageResource(R.mipmap.icon_mark_revision);
            holder.imgRev.setTag("Light=" + arrayQues.get(position).getQuestionID());
        }

        holder.tvQueID.setText(arrayQues.get(position).getQuestionID() + "");
        holder.tvQue.setText(arrayQues.get(position).getQuestion() + "");
        holder.tvans.setText("Answer : " + arrayQues.get(position).getAnswer());
        holder.imgRev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tagName = holder.imgRev.getTag().toString();
                String str[] = tagName.split("=");
                tagName = str[0];
                String strID = str[1];

                if (tagName.equalsIgnoreCase("Light")) {
                    holder.imgRev.setImageResource(R.mipmap.icon_fill_mark_revision);
                    holder.imgRev.setTag("Dark=" + strID);
                    dbp.updateRev(1, Integer.parseInt(holder.tvQueID.getText().toString()));
                    bq.setIsFavourite(1);
                } else {
                    holder.imgRev.setImageResource(R.mipmap.icon_mark_revision);
                    holder.imgRev.setTag("Light=" + strID);
                    dbp.updateRev(0, Integer.parseInt(holder.tvQueID.getText().toString()));
                    bq.setIsFavourite(0);
                }
                RevisionActivity.reload();
            }
        });
        return convertView;
    }
}
