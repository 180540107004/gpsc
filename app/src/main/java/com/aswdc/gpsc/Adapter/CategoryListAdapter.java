package com.aswdc.gpsc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.gpsc.Bean.BeanCategory;
import com.aswdc.gpsc.R;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.UserHolder>{

    Context context;
    ArrayList<BeanCategory> categoryList;
    ItemClickListener itemClickListener;
    View.OnClickListener onClickListener;

    // data is passed into the constructor
    public CategoryListAdapter(Context context,ArrayList<BeanCategory> categoryList,ItemClickListener itemClickListener) {
        this.context = context;
        this.categoryList= categoryList;
        this.itemClickListener =itemClickListener;
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    // inflates the row layout from .xml when needed
    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.row_category_list,null));

    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull UserHolder userHolder, final int pos) {
        userHolder.tvCategory.setText(categoryList.get(pos).getCategoryName());
       userHolder.categoryCard.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
                if(itemClickListener !=null){
                    itemClickListener.onItemClick(pos);
                }
           }
       });
    }

    // convenience method for getting data at click position
    public BeanCategory getItem(int id) {
        return categoryList.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener() {
        this.itemClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);
    }

    class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvCategory;
        ImageView ivCategory;
        CardView categoryCard;
        public UserHolder(@NonNull View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tv_CategoryName);
            ivCategory = itemView.findViewById(R.id.iv_CategoryList);
            categoryCard = itemView.findViewById(R.id.categoryCardView);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null)
                itemClickListener.onItemClick(getAdapterPosition());

        }
    }


}
