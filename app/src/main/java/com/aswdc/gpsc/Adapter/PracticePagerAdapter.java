package com.aswdc.gpsc.Adapter;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.Fragment;
import com.aswdc.gpsc.Bean.BeanPractice;
import com.aswdc.gpsc.Fragments.PracticeFragment;

import java.util.ArrayList;

public class PracticePagerAdapter extends FragmentPagerAdapter {

    ArrayList<BeanPractice> arrayque;
    public PracticePagerAdapter(FragmentManager fm, ArrayList<BeanPractice> arrayque) {
        super(fm);
        this.arrayque=arrayque;
    }

    @Override
    public int getCount() {
        return arrayque.size();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new PracticeFragment();
        Bundle args = new Bundle();
        args.putInt("page_position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "" + (position+1);
    }

}
