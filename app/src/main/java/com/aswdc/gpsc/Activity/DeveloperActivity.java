package com.aswdc.gpsc.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.gpsc.R;

public class DeveloperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);
    }
}
