package com.aswdc.gpsc.Activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.gpsc.Adapter.AdapterMarkRevision;
import com.aswdc.gpsc.Bean.BeanPractice;
import com.aswdc.gpsc.Database.DBPractice;
import com.aswdc.gpsc.Database.DB_MarkRev;
import com.aswdc.gpsc.R;

import java.util.ArrayList;

public class RevisionActivity extends AppCompatActivity {

    static ListView lstRev;
    static DB_MarkRev dbmr;
    static Activity act;
    static ArrayList<BeanPractice> arrayQue;
    BeanPractice bq;
    public static AdapterMarkRevision adp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revision);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;
        setTitle("Revision");

//        ActionBar supportActionBar = getSupportActionBar();
//        if (supportActionBar != null) {
//            supportActionBar.setDisplayHomeAsUpEnabled(true);
//        }

        lstRev = (ListView) findViewById(R.id.markRev_lst);
        dbmr = new DB_MarkRev(this);
        act = this;
        final DBPractice dbp = new DBPractice(this);
        bq = new BeanPractice();
        arrayQue = dbmr.getAllData();
        adp=new AdapterMarkRevision(this, arrayQue);
        lstRev.setAdapter(adp);


        /*lstRev.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               adp.notifyDataSetChanged();
//                ImageView imgRev= (ImageView) view.findViewById(R.id.markRev_img_rev);
//
//                String tagName = imgRev.getTag().toString();
//                String str[] = tagName.split("=");
//                tagName = str[0];
//                String strID = str[1];
//
//                if (tagName.equalsIgnoreCase("Light")) {
//                    imgRev.setImageResource(R.mipmap.ic_fill_mark_rev);
//                    imgRev.setTag("Dark=" + strID);
//                    dbp.updateRev(1, bq.getQuestionID());
//                    bq.setIsFavourite(1);
//                } else {
//                    imgRev.setImageResource(R.mipmap.ic_mark_rev);
//                    imgRev.setTag("Light=" + strID);
//                    dbp.updateRev(0, bq.getQuestionID());
//                    bq.setIsFavourite(0);
//                }
            }
        });*/
    }
    public static  void reload(){
        arrayQue = dbmr.getAllData();
        adp=new AdapterMarkRevision(act, arrayQue);
        lstRev.setAdapter(adp);
        adp.notifyDataSetChanged();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
