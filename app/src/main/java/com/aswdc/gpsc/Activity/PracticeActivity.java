package com.aswdc.gpsc.Activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.astuetz.PagerSlidingTabStrip;
import com.aswdc.gpsc.Adapter.PracticePagerAdapter;
import com.aswdc.gpsc.Database.DB_MarkRev;
import com.aswdc.gpsc.Fragments.PracticeFragment;
import com.aswdc.gpsc.R;
import com.aswdc.gpsc.Bean.BeanPractice;
import com.aswdc.gpsc.Database.DBPractice;
import java.util.ArrayList;

public class PracticeActivity extends AppCompatActivity {

    private PagerAdapter pPagerAdapter;
    static ViewPager vPage;
    public static ArrayList<BeanPractice> question_array;
    Button btnNext,btnLast,btnShowAnswer,btnPrevious,btnFirst;
    TextView tvAnswer;
    BeanPractice bpractice;
    DBPractice dbpractice;
    DB_MarkRev dbMarkRevision;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;
        setTitle("Practice");

        int categoryID = getIntent().getIntExtra("CategoryID",0);


        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        tvAnswer = (TextView) findViewById(R.id.practice_fragment_answer);
        btnShowAnswer = (Button) findViewById(R.id.practice_button_showAnswer);
        btnPrevious = (Button) findViewById(R.id.practice_button_previous);
        btnNext = (Button) findViewById(R.id.practice_button_next);
        vPage = (ViewPager) findViewById(R.id.practice_page);
        btnFirst= (Button) findViewById(R.id.practice_button_first);
        btnLast= (Button) findViewById(R.id.practice_button_last);
        bpractice = new BeanPractice();
        dbpractice = new DBPractice(this);
        dbMarkRevision = new DB_MarkRev(this);

        question_array = dbpractice.selectById(categoryID);
        pPagerAdapter = new PracticePagerAdapter(getSupportFragmentManager(), question_array);
        vPage.setAdapter(pPagerAdapter);
        pPagerAdapter.notifyDataSetChanged();

        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findViewById(R.id.practice_tabs);
        tabStrip.setViewPager(vPage);

        tabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                tvAnswer.setText("");
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous(0);
                vPage.setCurrentItem(0);
            }
        });

        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous(vPage.getCurrentItem() + 1);
                vPage.setCurrentItem(vPage.getCurrentItem() - 1);
            }
        });

        btnShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvAnswer.setVisibility(View.VISIBLE);
                int id = question_array.get(vPage.getCurrentItem()).getQuestionID();
                bpractice = dbpractice.selectQuestionByID(id);
                tvAnswer.setText(bpractice.getAnswer());

                PracticeFragment fragment=(PracticeFragment) vPage.getAdapter().instantiateItem(vPage, vPage.getCurrentItem());
                fragment.trueOption();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next(vPage.getCurrentItem() + 1);
                vPage.setCurrentItem(vPage.getCurrentItem() + 1);
            }
        });

        btnLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next(question_array.size()-1);
                vPage.setCurrentItem(question_array.size()-1);
            }
        });

        vPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                next(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void next(int position) {
        if (position == 0){
            btnPrevious.setVisibility(View.INVISIBLE);
            btnFirst.setVisibility(View.INVISIBLE);}
        else{
            btnPrevious.setVisibility(View.VISIBLE);
            btnFirst.setVisibility(View.VISIBLE);}
        if (position == question_array.size() - 1){
            btnNext.setVisibility(View.INVISIBLE);
            btnLast.setVisibility(View.INVISIBLE);}
        else{
            btnNext.setVisibility(View.VISIBLE);
            btnLast.setVisibility(View.VISIBLE);}
    }

    public void previous(int position) {
        if (position > 1){
            btnNext.setVisibility(View.VISIBLE);
            btnLast.setVisibility(View.VISIBLE);}
        if (position - 1 == 1){
            btnPrevious.setVisibility(View.INVISIBLE);
            btnFirst.setVisibility(View.INVISIBLE);}
        else{
            btnPrevious.setVisibility(View.VISIBLE);
            btnFirst.setVisibility(View.VISIBLE);}
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
