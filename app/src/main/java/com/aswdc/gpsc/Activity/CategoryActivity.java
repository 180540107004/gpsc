package com.aswdc.gpsc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.gpsc.Adapter.CategoryListAdapter;
import com.aswdc.gpsc.Bean.BeanCategory;
import com.aswdc.gpsc.Database.DBCategory;
import com.aswdc.gpsc.R;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity implements CategoryListAdapter.ItemClickListener{

    RecyclerView categoryRecyclerView;
    CardView categoryCardView;
    DBCategory dbCategory;
    CategoryListAdapter categoryListAdapter;
    ArrayList<BeanCategory> categoryArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        categoryRecyclerView = (RecyclerView) findViewById(R.id.categoryRecyclerView);
        categoryRecyclerView.setLayoutManager(new GridLayoutManager(this,1));
        categoryArrayList.addAll(new DBCategory(this).selectCategory());


        categoryListAdapter = new CategoryListAdapter(this, categoryArrayList,this);
        categoryRecyclerView.setAdapter(categoryListAdapter);
        categoryListAdapter.setClickListener();
    }

    @Override
    public void onItemClick(final int position) {
        categoryCardView = findViewById(R.id.categoryCardView);
        categoryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),PracticeActivity.class);
                intent.putExtra("CategoryID",categoryArrayList.get(position).getCategoryID());
                Toast.makeText(getApplicationContext(),"position"+position,Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }
}
