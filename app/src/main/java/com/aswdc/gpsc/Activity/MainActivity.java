package com.aswdc.gpsc.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.aswdc.gpsc.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    CardView btnPractice;
    CardView btnTest;
    CardView btnRevision;
    CardView btnDeveloper;
    FloatingActionButton btnShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPractice = (CardView) findViewById(R.id.activity_main_btn_practice);
        btnTest = (CardView) findViewById(R.id.activity_main_btn_test);
        btnRevision = (CardView) findViewById(R.id.activity_main_btn_revision);
        btnDeveloper = (CardView) findViewById(R.id.activity_main_btn_developer);
        btnShare = (FloatingActionButton) findViewById(R.id.activity_main_button_share);

        btnPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, CategoryActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });

        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, TestActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });

        btnRevision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, RevisionActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });
        btnDeveloper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, DeveloperActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent();
                share.setAction("android.intent.action.SEND");
                share.setType("text/plain");
                share.putExtra("android.intent.extra.TEXT", "Download GTU MCQ App");
                startActivity(share);
            }
        });

    }

}